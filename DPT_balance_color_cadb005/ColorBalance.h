/*
 * ColorBalance.h
 *
 *  Created on: 2015年4月23日
 *      Author: LuoJing-Yuan
 */

#ifndef COLORBALANCE_H
#define COLORBALANCE_H

#include <vector>
#include <queue>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "Window.h"
#include "Position.h"
#include <math.h>
#include <float.h>


class Component {
  public:

    int mID ;
    int mPixel ;
    Position * mPosition ;

    Component( int id, Position * position ) ;

} ;

class ColorList {
  public :
    int mSize ;
    std::vector<int> mColor ;

    double mDiff ;
    bool initial ;

  ColorList( int numOfItem ) {
    for ( int i = 0 ; i < numOfItem ; i++  )
       mColor.push_back(NOCOLOR) ;
    mDiff = DBL_MAX ;
    initial = true ;
    mSize = numOfItem ;

  }  // ColorList

};  // class colorList

class ColorBalance {


  public:
    std::vector< Vertex * > * mVertexList ;
    std::vector< Component * > * mComponentList ;
    std::vector< std::vector< Vertex * > * > * mGroupList ;
    std::vector<int> * mEnableColorList ;

    int mWindowSize ;
    int mXOfWindow ;
    int mYOfWindow ;

    Window * mWindowList ;

    Position * mWindowBound ;  // array size is 4

    ColorList * mBestList ;

    ColorList * mNowList ;

  private:
    int mALPHA ;
    int mBETA ;
    int mOMEGA ;

    int * mSortEdge[ 4 ] ; // from LEFT & DOWN to RIGHT & UP

    void BuildGraphic( int ALPHA, int BETA ) ;

    void SetGroup( int groupID, int VertexID, bool * haveGroup ) ;

    void SortGroup() ;

  public:
    ColorBalance( std::vector< Component * > * CL, int ALPHA, int BETA, int OMEGA ) ;

    void StartVisitVertex( bool isFirstTime, int type ) ;

    void DoOptimizeColorBalance( int type ) ;

    void SetBoundingBox() ;

    void CopyColorList() ;

    Position * GetWindowBound() ;

    void CalculateWindowDensity() ;

} ;

#endif /* COLORBALANCE_H */
