/*
 * Graphic.cpp
 *
 *  Created on: 2015年4月23日
 *      Author: LuoJing-Yuan
 */

#include "ColorBalance.h"

Component::Component( int id, Position * position ) {
  mID = id ;
  mPosition = position ;
  mPixel = ( position[ RIGHT_UP ].X - position[ LEFT_DOWN ].X )
      * ( position[ RIGHT_UP ].Y - position[ LEFT_DOWN ].Y ) ;
}  // Component::Component

ColorBalance::ColorBalance( std::vector< Component* > * CL, int ALPHA, int BETA,
    int OMEGA ) {

  mALPHA = ALPHA ;

  mBETA = BETA ;

  mOMEGA = OMEGA ;

  mVertexList = new std::vector< Vertex* >() ;

  mComponentList = CL ;

  mGroupList = new std::vector< std::vector< Vertex * > * >() ;

  mWindowBound = NULL ;

  mWindowList = NULL ;

  mWindowSize = 0 ;

  mXOfWindow = 0 ;

  mYOfWindow = 0 ;

  mEnableColorList = NULL ;

  mBestList = NULL ;

  mNowList = NULL ;

  for ( int i = 0; i < CL->size(); i++ )
    mVertexList->push_back( new Vertex( i ) ) ;

  mWindowBound = new Position[ 4 ] ;

  this->BuildGraphic( ALPHA, BETA ) ;

} // ColorBalance::Graphic()

static int * SortingEdge( std::vector< Component* > * CL, int EdgeID ) {

  int size = CL->size() ;
  int * arrayEdge = new int[ size ] ;
  int tempArray[ size ] ;
  bool havePick[ size ] ;

  memset( &havePick, false, size * sizeof(bool) ) ; // array initial
  memset( arrayEdge, 0, size * sizeof(int) ) ;

  if ( EdgeID == LEFT )
    for ( int i = 0; i < size; i++ )
      tempArray[ i ] = ( *CL )[ i ]->mPosition[ LEFT_DOWN ].X ;
  else if ( EdgeID == DOWN )
    for ( int i = 0; i < size; i++ )
      tempArray[ i ] = ( *CL )[ i ]->mPosition[ LEFT_DOWN ].Y ;
  else if ( EdgeID == RIGHT )
    for ( int i = 0; i < size; i++ )
      tempArray[ i ] = ( *CL )[ i ]->mPosition[ RIGHT_UP ].X ;
  else if ( EdgeID == UP )
    for ( int i = 0; i < size; i++ )
      tempArray[ i ] = ( *CL )[ i ]->mPosition[ RIGHT_UP ].Y ;

  for ( int i = size - 1; i >= 0; i-- ) {
    int value = INT_MIN, select = 0, j = 0 ; // initial
    for ( ; j < size; j++ ) {
      if ( !havePick[ j ] && tempArray[ j ] > value ) {
        select = j ;
        value = tempArray[ j ] ;
      } // if
    }  // for

    arrayEdge[ i ] = select ;
    havePick[ select ] = true ;

  } // for

  return arrayEdge ;
}  // sortingEdge()

void ColorBalance::BuildGraphic( int ALPHA, int BETA ) {

  int size = mComponentList->size() ;

  mSortEdge[ LEFT ] = SortingEdge( mComponentList, LEFT ) ;
  mSortEdge[ DOWN ] = SortingEdge( mComponentList, DOWN ) ;
  mSortEdge[ UP ] = SortingEdge( mComponentList, UP ) ;
  mSortEdge[ RIGHT ] = SortingEdge( mComponentList, RIGHT ) ;

  // start of setting neighbor
  // visit from left to right

  for ( int i = 0; i < size - 1 /* final not do so -1 */; i++ ) { // ALPHA is horizontal

    Component * center = ( *mComponentList )[ mSortEdge[ LEFT ][ i ] ] ;

    for ( int k = 1, j = mSortEdge[ LEFT ][ i + k ]; i + k < size; k++, j =
        mSortEdge[ LEFT ][ i + k ] ) {
      Component * tempCompoenet = mComponentList->at( j ) ;
      int dis = tempCompoenet->mPosition[ LEFT_UP ].X
          - center->mPosition[ RIGHT_UP ].X ;

      if ( dis > 0 && dis < ALPHA ) {  // too close

        if ( center->mPosition[ RIGHT_UP ].Y
            - tempCompoenet->mPosition[ RIGHT_DOWN ].Y > 0
            && tempCompoenet->mPosition[ RIGHT_UP ].Y
                - center->mPosition[ RIGHT_DOWN ].Y > 0 ) {
          ( *mVertexList )[ center->mID ]->mCV.push_back( tempCompoenet->mID ) ;
          ( *mVertexList )[ tempCompoenet->mID ]->mCV.push_back( center->mID ) ;
        }  // if

      }  // if

      else if ( dis >= ALPHA )
        break ;
    }  // for
  } // for

  // visit from down to up

  for ( int i = 0; i < size - 1; i++ ) { // ALPHA is horizontal

    Component * center = mComponentList->at( mSortEdge[ DOWN ][ i ] ) ;

    for ( int k = 1, j = mSortEdge[ DOWN ][ i + k ]; i + k < size; k++, j =
        mSortEdge[ DOWN ][ i + k ] ) {
      Component * tempCompoenet = mComponentList->at( j ) ;

      int dis = tempCompoenet->mPosition[ RIGHT_DOWN ].Y
          - center->mPosition[ RIGHT_UP ].Y ;
      if ( dis > 0 && dis < BETA ) {  // too close
        if ( center->mPosition[ RIGHT_UP ].X
            - tempCompoenet->mPosition[ LEFT_UP ].X > 0
            && tempCompoenet->mPosition[ RIGHT_UP ].X
                - center->mPosition[ LEFT_UP ].X > 0 ) {
          ( *mVertexList )[ center->mID ]->mCV.push_back( tempCompoenet->mID ) ;
          ( *mVertexList )[ tempCompoenet->mID ]->mCV.push_back( center->mID ) ;
        }  // if
      }  // if

      else if ( dis >= BETA )
        break ;

    }  // for
  }  // for

  /*
   for ( int i = 0 ; i < size ; i++ ) {
   printf( "item %d: < ", mVertexList->at(i)->mID+1 ) ;
   for ( int j = 0 ; j < mVertexList->at(i)->mCV.size() ; j++ )
   printf( "%d ", mVertexList->at(i)->mCV.at(j)+1 ) ;
   printf( ">\n" ) ;
   }
   */
  for ( int i = 0; i < size; i++ ) {
    ( *mVertexList )[ i ]->mNumOfNeighbor = ( *mVertexList )[ i ]->mCV.size() ;
  }  // for

  // End of setting neighbor

  // start of setting group

  bool haveDone = false ;

  bool * haveGroup = new bool[ size ] ;
  memset( haveGroup, false, size * sizeof(bool) ) ; // array initial

  for ( int i = 0, j = 0; !haveDone; i++ ) {
    haveDone = true ; //assume this round will finish ;
    mGroupList->push_back( new std::vector< Vertex* >() ) ;
    for ( j = 0 ; j < size; j++ ) {
      if ( !haveGroup[ j ] ) {
        this->SetGroup( i, j, haveGroup ) ;
        break ;
      }  // if

    }  // for
    for ( j = 0 ; j < size; j++ ) {
      if ( !haveGroup[ j ] ) {
        haveDone = false ;
        break ;
      }  // if
    } // for
  } // for

  /*
   for ( int i = 0; i < mGroupList->size(); i++ ) {
   printf( "Group %d: < ", i + 1 ) ;
   for ( int j = 0; j < mGroupList->at( i )->size(); j++ )
   printf( "%d ", mGroupList->at( i )->at( j )->mID + 1 ) ;
   printf( "> \n" ) ;
   } // for
   */

  // End of setting group

}  // ColorBalance::BuildGraphic()

void ColorBalance::SetGroup( int groupID, int vertexID, bool * haveGroup ) {

  mGroupList->at( groupID )->push_back( mVertexList->at( vertexID ) ) ;
  haveGroup[ vertexID ] = true ;

  for ( int i = 0; i < mVertexList->at( vertexID )->mCV.size(); i++ ) {
    int index = mVertexList->at( vertexID )->mCV.at( i ) ;
    if ( !haveGroup[ index ] )
      this->SetGroup( groupID, index, haveGroup ) ;
  }  // for

}  // ColorBalance::SetGroup()

Position * ColorBalance::GetWindowBound() {
  return mWindowBound ;
}  // ColorBalance::GetWindowBoind()

void ColorBalance::StartVisitVertex( bool isFirstTime, int type ) {

  int numOfGroup = mGroupList->size() ;

  if ( isFirstTime ) {

    mEnableColorList = new std::vector< int > ;
    // visit each group
    for ( int i = 0; i < numOfGroup; i++ ) {

      //printf( "Group: %d  \n", i ) ;
      // select the start vertex ( biggest number of vertex )
      int sizeOfGroup = ( *mGroupList )[ i ]->size() ;
      int select = 0 ;
      for ( int j = 1; j < sizeOfGroup; j++ ) {
        if ( mComponentList->at( ( *( *mGroupList )[ i ] )[ select ]->mID )->mPixel
            < mComponentList->at( ( *( *mGroupList )[ i ] )[ j ]->mID )->mPixel )
          select = j ;
      }  // for

      // set color

      std::queue< int > * queue = new std::queue< int >() ;
      int color = RED ;
      ( *queue ).push( ( *( *mGroupList )[ i ] )[ select ]->mID ) ;

      while ( !queue->empty() ) {
        std::queue< int > * tempQueue = new std::queue< int >() ;
        for ( Vertex * node = ( *mVertexList )[ ( *queue ).front() ]; true;
            node = ( *mVertexList )[ ( *queue ).front() ] ) {

          if ( !node->mHavaPass ) {
            // printf( "%d:%d \n", node->mID + 1, color ) ;
            node->mColor = color ;
            node->mHavaPass = true ;

            for ( int j = 0; j < node->mCV.size(); j++ ) {
              ( *tempQueue ).push( node->mCV[ j ] ) ;
            } // for
          } // if
          ( *queue ).pop() ;
          if ( ( *queue ).empty() )
            break ;
        }  // for

        color = ( color == RED ) ? BLUE : RED ;
        free( queue ) ;
        queue = tempQueue ;
      } // while
      free( queue ) ;
      //  End of setting color

      //  start checking cycle

      bool dontNeedColor = false ;

      for ( int j = 0; j < ( *mGroupList )[ i ]->size(); j++ ) {
        for ( int k = 0; k < mGroupList->at( i )->at( j )->mCV.size(); k++ )
          if ( mGroupList->at( i )->at( j )->mColor
              == mVertexList->at( mGroupList->at( i )->at( j )->mCV.at( k ) )->mColor ) {
            dontNeedColor = true ;
            break ;
          } // if
        if ( dontNeedColor )
          break ;
      } // for

      //( dontNeedColor ) ? printf( "NO\n\n" ) : printf( "YES\n\n" ) ;
      if ( dontNeedColor )
        for ( int j = 0; j < ( *mGroupList )[ i ]->size(); j++ )
          ( *( *mGroupList )[ i ] )[ j ]->mColor = NOCOLOR ;
      else
        mEnableColorList->push_back( i ) ;

    }  // for
    this->SetBoundingBox() ;
    mBestList = new ColorList( numOfGroup ) ;
    mNowList = new ColorList( numOfGroup ) ;


  } // if
  else if ( !isFirstTime ) {
    if ( type == 0 || type == 1 ) {
      SortGroup() ;
      int lastColorA = BLUE ;
      int lastColorB = RED ;

      for ( int index = 0; index < mEnableColorList->size(); index++ ) {
        // printf( "\n" ) ;
        double ColorA_diff = 0.0 ;
        double ColorB_diff = 0.0 ;
        int i = ( *mEnableColorList )[ index ] ;

        // setdefault color
        if ( rand() % 2  == 0 ) {
          lastColorA = ( lastColorA == RED ) ? BLUE : RED ;
          lastColorB = ( lastColorB == RED ) ? BLUE : RED ;
        } // if

        for ( int time = 0; time < 3; time++ ) {
          int sizeOfGroup = ( *mGroupList )[ i ]->size() ;
          int select = 0 ;
          for ( int j = 1; j < sizeOfGroup; j++ ) {
            if ( mComponentList->at( ( *( *mGroupList )[ i ] )[ select ]->mID )->mPixel
                < mComponentList->at( ( *( *mGroupList )[ i ] )[ j ]->mID )->mPixel )
              select = j ;
          }  // for

          std::queue< int > queue ;

          int color = ( time == 0 || time == 2 ) ? lastColorA : lastColorB ;

          int startID = ( *( *mGroupList )[ i ] )[ select ]->mID ;

          queue .push( startID ) ;

          while ( !queue.empty() ) {
            std::queue< int > tempQueue  ;
            for ( Vertex * node = ( *mVertexList )[ queue.front() ]; true;
                node = ( *mVertexList )[ queue.front() ] ) {

              if ( !node->mHavaPass ) {
                // printf( "%d:%d \n", node->mID + 1, color ) ;
                node->mColor = color ;
                node->mHavaPass = true ;

                for ( int j = 0; j < node->mCV.size(); j++ ) {
                  tempQueue.push( node->mCV[ j ] ) ;
                } // for
              } // if
              queue.pop() ;
              if ( queue.empty() )
                break ;
            }  // for

            color = ( color == RED ) ? BLUE : RED ;
            queue = tempQueue ;
          } // while


          if ( time < 2 )
            this->CalculateWindowDensity() ;

          if ( time == 0 )
            for ( int ii = 0; ii < mWindowSize; ii++ )
              ColorA_diff += mWindowList[ ii ].mColorDiff ;
          else if ( time == 1 )
            for ( int ii = 0; ii < mWindowSize; ii++ )
              ColorB_diff += mWindowList[ ii ].mColorDiff ;

          // printf( "Group: %d\nRED: %f\nBLUE: %f\n", i + 1, ColorA_diff, ColorB_diff ) ;
          if ( time == 1 && ColorA_diff > ColorB_diff ) {
              mNowList->mColor[ i ] = lastColorB ;
              mNowList->mDiff = ColorB_diff ;
              break ;
          } // if

          if ( time == 0 || time == 1 ) {
            for ( int j = 0; j < ( *mGroupList )[ i ]->size(); j++ ) {
              ( *( *mGroupList )[ i ] )[ j ]->initial( 0 ) ;
            } // for
          } // if

          else {
            mNowList->mColor[ i ] = lastColorA ;
            mNowList->mDiff = ColorA_diff ;
          } // else
        }  // for
      }  // for
    } // if

    else if ( type == 2 ) {

      for ( int index = 0; index < mEnableColorList->size(); index++ ) {

        int i = ( *mEnableColorList )[ index ] ;

        int sizeOfGroup = ( *mGroupList )[ i ]->size() ;
        int select = 0 ;
        for ( int j = 1; j < sizeOfGroup; j++ ) {
          if ( mComponentList->at( ( *( *mGroupList )[ i ] )[ select ]->mID )->mPixel
              < mComponentList->at( ( *( *mGroupList )[ i ] )[ j ]->mID )->mPixel )
            select = j ;
        }  // for


        std::queue< int > queue ;

        int color = mBestList->mColor[i] ;
        //printf( "start: %d %d\n",i, color ) ;

        int startID = ( *( *mGroupList )[ i ] )[ select ]->mID ;

        queue .push( startID ) ;

        while ( !queue.empty() ) {
          std::queue< int > tempQueue  ;
          for ( Vertex * node = ( *mVertexList )[ queue.front() ]; true;
              node = ( *mVertexList )[ queue.front() ] ) {

            if ( !node->mHavaPass ) {
              // printf( "%d:%d \n", node->mID + 1, color ) ;
              node->mColor = color ;
              node->mHavaPass = true ;

              for ( int j = 0; j < node->mCV.size(); j++ ) {
                tempQueue.push( node->mCV[ j ] ) ;
              } // for
            } // if
            queue.pop() ;
            if ( queue.empty() )
              break ;
          }  // for

          color = ( color == RED ) ? BLUE : RED ;
          queue = tempQueue ;
        } // while
      }// for

    }  // else
  }  // else if

}  // ColorBalance::StartVisitVertex()

void ColorBalance::SetBoundingBox() {

  // set bounding box

  int size = mVertexList->size() ;

  for ( int i = 0; i < size; i++ )
    if ( ( *mVertexList )[ mSortEdge[ LEFT ][ i ] ]->mColor != NOCOLOR ) {
      mWindowBound[ LEFT_DOWN ].X =
          ( *mComponentList )[ mSortEdge[ LEFT ][ i ] ]->mPosition[ LEFT_DOWN ].X ;
      break ;
    } // if

  for ( int i = 0; i < size; i++ )
    if ( ( *mVertexList )[ mSortEdge[ DOWN ][ i ] ]->mColor != NOCOLOR ) {
      mWindowBound[ LEFT_DOWN ].Y =
          ( *mComponentList )[ mSortEdge[ DOWN ][ i ] ]->mPosition[ LEFT_DOWN ].Y ;
      break ;
    } // if

  for ( int i = 0; i < size; i++ )
    if ( ( *mVertexList )[ mSortEdge[ RIGHT ][ size - i - 1 ] ]->mColor
        != NOCOLOR ) {
      mWindowBound[ RIGHT_UP ].X = ( *mComponentList )[ mSortEdge[ RIGHT ][ size
          - i - 1 ] ]->mPosition[ RIGHT_UP ].X ;
      break ;
    } // if

  for ( int i = 0; i < size; i++ )
    if ( ( *mVertexList )[ mSortEdge[ UP ][ size - i - 1 ] ]->mColor
        != NOCOLOR ) {
      mWindowBound[ RIGHT_UP ].Y = ( *mComponentList )[ mSortEdge[ UP ][ size
          - i - 1 ] ]->mPosition[ RIGHT_UP ].Y ;
      break ;
    } // if

  mWindowBound[ RIGHT_DOWN ].X = mWindowBound[ RIGHT_UP ].X ;
  mWindowBound[ RIGHT_DOWN ].Y = mWindowBound[ LEFT_DOWN ].Y ;

  mWindowBound[ LEFT_UP ].X = mWindowBound[ LEFT_DOWN ].X ;
  mWindowBound[ LEFT_UP ].Y = mWindowBound[ RIGHT_UP ].Y ;
  /*
   printf( "%d %d %d %d", mWindowBound[ LEFT_DOWN ].X,
   mWindowBound[ LEFT_DOWN ].Y, mWindowBound[ RIGHT_UP ].X,
   mWindowBound[ RIGHT_UP ].Y ) ;
   */
}  // ColorBalance::SetBoundingBox()

void ColorBalance::CalculateWindowDensity() {

  int lenth_x = mWindowBound[ RIGHT_UP ].X - mWindowBound[ LEFT_DOWN ].X ;
  int lenth_y = mWindowBound[ RIGHT_UP ].Y - mWindowBound[ LEFT_DOWN ].Y ;

  // printf( "%d %d\n", lenth_x, lenth_y ) ;
  // printf( "%d\n", mOMEGA ) ;
  int numOfX = lenth_x / mOMEGA ;
  if ( lenth_x % mOMEGA != 0 )
    numOfX += 1 ;

  int numOfY = lenth_y / mOMEGA ;
  if ( lenth_y % mOMEGA != 0 )
    numOfY += 1 ;

  // printf( "%d %d\n", numOfX , numOfY ) ;

  // calculate Density window
  mXOfWindow = numOfX ;
  mYOfWindow = numOfY ;
  mWindowSize = numOfX * numOfY ;

  delete [] mWindowList ;
  mWindowList = new Window[ mWindowSize ] ;

  if ( numOfX == 1 && numOfY == 1 ) {
    mWindowList[ 0 ].Setting( mWindowBound[ LEFT_DOWN ].X,
        mWindowBound[ LEFT_DOWN ].Y, mWindowBound[ LEFT_DOWN ].X + mOMEGA,
        mWindowBound[ LEFT_DOWN ].Y + mOMEGA ) ;
  } // if

  else {
    for ( int Y = mWindowBound[ LEFT_DOWN ].Y + mOMEGA, i = 0; true; Y +=
        mOMEGA, i++ ) {
      for ( int X = mWindowBound[ LEFT_DOWN ].X + mOMEGA; true;
          i++, X += mOMEGA ) {
        bool OutBoundx = !( X < mWindowBound[ RIGHT_UP ].X ) ;
        bool OutBoundy = !( Y < mWindowBound[ RIGHT_UP ].Y ) ;
        if ( !OutBoundx && !OutBoundy ) {
          mWindowList[ i ].Setting( X - mOMEGA, Y - mOMEGA, X, Y ) ;
        }  // if

        else if ( !OutBoundx && OutBoundy ) {
          mWindowList[ i ].Setting( X - mOMEGA,
              mWindowBound[ RIGHT_UP ].Y - mOMEGA, X,
              mWindowBound[ RIGHT_UP ].Y ) ;
        }  // else if

        else if ( OutBoundx && !OutBoundy ) {
          mWindowList[ i ].Setting( mWindowBound[ RIGHT_UP ].X - mOMEGA,
              Y - mOMEGA, mWindowBound[ RIGHT_UP ].X, Y ) ;
          break ;
        }  // else if

        else {
          mWindowList[ i ].Setting( mWindowBound[ RIGHT_UP ].X - mOMEGA,
              mWindowBound[ RIGHT_UP ].Y - mOMEGA, mWindowBound[ RIGHT_UP ].X,
              mWindowBound[ RIGHT_UP ].Y ) ;
          break ;
        }  // else
      }  // for
      if ( !( Y < mWindowBound[ RIGHT_UP ].Y ) )
        break ;
    } // for
  } // else

  /*
   for ( int i = 0; i < mWindowSize; i++ ) {
   printf( "%d %d %d %d\n", mWindowList[ i ].mPosition[ 0 ]->X,
   mWindowList[ i ].mPosition[ 0 ]->Y, mWindowList[ i ].mPosition[ 1 ]->X,
   mWindowList[ i ].mPosition[ 1 ]->Y ) ;
   }  // for
   */

  // calculate pixel of component  in window
  int vertexSize = mVertexList->size() ;

  for ( int i = 0; i < vertexSize; i++ ) {
    for ( int j = 0; j < mWindowSize; j++ ) {
      int x1, y1, x2, y2, wx1, wx2, wy1, wy2, vx1, vx2, vy1, vy2 ;
      Vertex * Vtemp = ( *mVertexList )[ i ] ;
      Component * Ctemp = ( *mComponentList )[ Vtemp->mID ] ;
      if ( Vtemp->mColor != NOCOLOR ) {
        wx1 = mWindowList[ j ].mPosition[ 0 ]->X ;
        wx2 = mWindowList[ j ].mPosition[ 1 ]->X ;

        wy1 = mWindowList[ j ].mPosition[ 0 ]->Y ;
        wy2 = mWindowList[ j ].mPosition[ 1 ]->Y ;

        vx1 = Ctemp->mPosition[ LEFT_DOWN ].X ;
        vx2 = Ctemp->mPosition[ RIGHT_UP ].X ;

        vy1 = Ctemp->mPosition[ LEFT_DOWN ].Y ;
        vy2 = Ctemp->mPosition[ RIGHT_UP ].Y ;

        x1 = ( wx1 >= vx1 ) ? wx1 : vx1 ;
        x2 = ( vx2 >= wx2 ) ? wx2 : vx2 ;
        y1 = ( wy1 >= vy1 ) ? wy1 : vy1 ;
        y2 = ( vy2 >= wy2 ) ? wy2 : vy2 ;

        if ( x2 - x1 > 0 && y2 - y1 > 0 ) {
          int color = ( x2 - x1 ) * ( y2 - y1 ) ;
          if ( ( *mVertexList )[ i ]->mColor == RED )
            mWindowList[ j ].mColorA += color ;

          else if ( ( *mVertexList )[ i ]->mColor == BLUE )
            mWindowList[ j ].mColorB += color ;
        } // if
      }  // for

    }  // for
  } // if

  for ( int i = 0; i < mWindowSize; i++ ) {
    int x = mWindowList[ i ].mPosition[ 1 ]->X
        - mWindowList[ i ].mPosition[ 0 ]->X ;

    int y = mWindowList[ i ].mPosition[ 1 ]->Y
        - mWindowList[ i ].mPosition[ 0 ]->Y ;

    mWindowList[ i ].mColorDensityA = 100
        * ( ( float ) ( mWindowList[ i ].mColorA ) / ( float ) ( x * y ) ) ;
    mWindowList[ i ].mColorDensityB = 100
        * ( ( float ) ( mWindowList[ i ].mColorB ) / ( float ) ( x * y ) ) ;

    mWindowList[ i ].mColorDiff = fabs(
        ( double ) ( mWindowList[ i ].mColorDensityA
            - mWindowList[ i ].mColorDensityB ) ) ;
  }  // for

  // for ( int i = 0; i < 4; i++ )
  //  printf(  "%4.2f\t%4.2f\n", mWindowList[ i ].mColorDensityA, mWindowList[ i ].mColorDensityB ) ;

} // ColorBalance::CalculaateWindowDensity()

void ColorBalance::DoOptimizeColorBalance( int type ) {

  // initial color in vertex

  for ( int i = 0; i < mGroupList->size(); i++ ) {
    for ( int j = 0; j < ( *mGroupList )[ i ]->size(); j++ ) {
      if ( type != 1 )
        ( *( *mGroupList )[ i ] )[ j ]->initial( 0 ) ;
      else
        ( *( *mGroupList )[ i ] )[ j ]->initial( 1 ) ;
    } // for
  } // for

  StartVisitVertex( false, type ) ;

} // ColorBalance::DoOptimizeColoBalance()

void ColorBalance::SortGroup() {

  int GroupPixel[ mEnableColorList->size() ] ;
  std::vector< int > * tempList = new std::vector< int > ;
  for ( int i = 0, groupindex = mEnableColorList->at( 0 );
      i < mEnableColorList->size(); i++ ) {
    groupindex = mEnableColorList->at( i ) ;
    int sizeOfGroup = ( *mGroupList )[ groupindex ]->size() ;
    GroupPixel[ i ] = 0 ;
    for ( int j = 0; j < sizeOfGroup; j++ )
      if ( mComponentList->at( ( *( *mGroupList )[ groupindex ] )[ j ]->mID )->mPixel
          > GroupPixel[ i ] )
        GroupPixel[ i ] = mComponentList->at(
            ( *( *mGroupList )[ groupindex ] )[ j ]->mID )->mPixel ;
  } // for

  for ( int k = 0; k < mEnableColorList->size(); k++ ) {
    int select = 0 ;
    for ( int i = 1; i < mEnableColorList->size(); i++ )
      if ( GroupPixel[ i ] > GroupPixel[ select ] )
        select = i ;
    tempList->push_back( mEnableColorList->at( select ) ) ;
    GroupPixel[ select ] = -1 ;
  } // for
  delete mEnableColorList ;
  mEnableColorList = tempList ;

} // ColorBalance::SortGroup()

void ColorBalance::CopyColorList() {
  if ( ( mBestList->mDiff > mNowList->mDiff ) ) {
    mBestList->initial = false ;
    for ( int i = 0; i < mBestList->mSize; i++ )
      mBestList->mColor[ i ] = mNowList->mColor[ i ] ;

    mBestList->mDiff = mNowList->mDiff ;
  }  // if
}  // ColorBalance::CopyColorList()

