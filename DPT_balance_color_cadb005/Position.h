/*
 * Position.h
 *
 *  Created on: 2015年6月9日
 *      Author: LuoJing-Yuan
 */

#ifndef SRC_POSITION_H_
#define SRC_POSITION_H_


struct Position {
    int X ;
    int Y ;
    Position() { X = 0; Y = 0 ; }// Position
    Position( int x1, int y1 ) {
      X = x1 ;
      Y = y1 ;
    }  // Position
} ;

enum EDGE {
  LEFT = 0, DOWN = 1, RIGHT = 2, UP = 3
} ;

enum POSITION {
  LEFT_DOWN = 0, LEFT_UP = 1, RIGHT_UP = 2, RIGHT_DOWN = 3,
} ;

enum COLOR {
  NOCOLOR = 0, RED = 1, BLUE = 2
} ;

struct Vertex {
  public:
    int mID ;
    int mColor ;
    int mNumOfNeighbor ;
    std::vector< int > mCV ;  // connect Vertex
    bool mHavaPass ;

    Vertex( int id ) {
      mID = id ;
      mColor = NOCOLOR ;
      mHavaPass = false ;
      mNumOfNeighbor = 0 ;
    } // Vertex

    void initial( int type ) {
      if ( type == 0 )mColor = NOCOLOR ;
      mHavaPass = false ;
    }  // initial()

} ;


#endif /* SRC_POSITION_H_ */
