/*
 * Window.h
 *
 *  Created on: 2015年6月9日
 *      Author: LuoJing-Yuan
 */

#ifndef SRC_WINDOW_H_
#define SRC_WINDOW_H_

#include "Position.h"

class Window {
  public:
    Position * mPosition[2] ;
    int mColorA ;
    int mColorB ;

    float mColorDensityA ;
    float mColorDensityB ;

    double mColorDiff ;

    void Setting(  int x1, int y1, int x2, int y2 ) {
      mPosition[0] = new Position( x1, y1) ;
      mPosition[1] = new Position( x2, y2) ;
      mColorA = mColorB = 0 ;
      mColorDensityA = mColorDensityB = 0.0 ;
      mColorDiff = 0.0 ;
    } // windows
    ~Window() {
      delete mPosition[0] ;
      delete mPosition[1] ;
    }  // Window
};


#endif /* SRC_WINDOW_H_ */
