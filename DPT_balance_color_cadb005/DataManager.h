/*
 * DataManager.h
 *
 *  Created on: 2015年4月23日
 *      Author: LuoJing-Yuan
 */

#ifndef DATAMANAGER_H_
#define DATAMANAGER_H_

#include <string>
#include "ColorBalance.h"
#include <stdio.h>
#include <stdlib.h>




class IOUnit {

  public:

    IOUnit( std::string * inputFilePath ) ;

    ColorBalance * InputData( int & ALPHA, int & BETA, int & OMEGA ) ;

    void OutputData( std::string * output ) ;

    void SetOutputFilePath( std::string * outputPath ) ;
    private:

    FILE * mInputFile ;
    std::string * mOutputPath ;

} ;



class DataManager {
  private:

    int mALPHA ;
    int mBETA ;
    int mOMEGA ;

  public:
    IOUnit * mIO_Unit ;

    ColorBalance * mColorBalance ;

    DataManager( std::string * inputFilePath ) ;

    IOUnit * GetIO_Unit() ;

    ColorBalance * GetGraphic() ;

    void PrintData() ;

} ;


#endif /* DATAMANAGER_H_ */
