/*
 * DataManager.cpp
 *
 *  Created on: 2015年4月23日
 *      Author: LuoJing-Yuan
 */

#include "DataManager.h"

#define DEBUG
#ifdef DEBUG
   std::string * resultFile = new std::string ;
#endif


void IOUnit::SetOutputFilePath( std::string * outputPath ) {

  mOutputPath = outputPath ;
#ifdef DEBUG
    resultFile->append( outputPath->c_str()  ) ;
    resultFile->append( "_data.out" ) ;
#endif

}

IOUnit::IOUnit( std::string * inputFilePath ) {

  mInputFile = fopen( inputFilePath->c_str(), "r" ) ;
  mOutputPath = NULL ;

}  // IOUnit::IOUnit()

ColorBalance * IOUnit::InputData( int & ALPHA, int & BETA, int & OMEGA ) {

  std::vector< Component * > * ComponentList =
      new std::vector< Component * >() ;

  char str[ 20 ] ;
  char charNum[ 10 ] ;
  fscanf( mInputFile, "%s", str ) ;
  
  strcpy( charNum, str + 6 ) ;
  ALPHA = atoi( charNum ) ;

  fscanf( mInputFile, "%s", str ) ;
  strcpy( charNum, str + 5 ) ;
  BETA = atoi( charNum ) ;

  fscanf( mInputFile, "%s", str ) ;
  strcpy( charNum, str + 6 ) ;
  OMEGA = atoi( charNum ) ;

  for ( int id = 0; !feof( mInputFile ); id++ ) {
    fscanf( mInputFile, "%s", str ) ;
    if ( feof( mInputFile ) ) break ;
    Position * position = new Position[ 4 ] ;

    int strLength = strlen( str ) ;
    for ( int strIndex = 0, numStrIndex = 0, commaIndex = 0;
        strIndex < strLength + 1; strIndex++, numStrIndex++ ) {

      if ( str[ strIndex ] == ',' || strIndex == strLength ) {
        charNum[ numStrIndex ] = '\0' ;

        if ( commaIndex == 0 ) {

          position[ LEFT_DOWN ].X = atoi( charNum ) ;
          position[ LEFT_UP ].X = position[ LEFT_DOWN ].X ;

        }  // if

        else if ( commaIndex == 1 ) {

          position[ LEFT_DOWN ].Y = atoi( charNum ) ;
          position[ RIGHT_DOWN ].Y = position[ LEFT_DOWN ].Y ;

        }  // else if

        else if ( commaIndex == 2 ) {

          position[ RIGHT_UP ].X = atoi( charNum ) ;
          position[ RIGHT_DOWN ].X = position[ RIGHT_UP ].X ;

        }  // else if

        else if ( commaIndex == 3 ) {

          position[ RIGHT_UP ].Y = atoi( charNum ) ;
          position[ LEFT_UP ].Y = position[ RIGHT_UP ].Y ;

        }  // else if

        numStrIndex = -1 ;
        commaIndex++ ;

      }  // if

      else
        charNum[ numStrIndex ] = str[ strIndex ] ;

    }  // for

    ComponentList->push_back( new Component( id, position ) ) ;

  }  // for

  return new ColorBalance( ComponentList, ALPHA, BETA, OMEGA ) ;
} // IOUnit::InputData()

void IOUnit::OutputData( std::string * output ) {

  FILE * outputfile = fopen( mOutputPath->c_str(), "w" ) ;
  fprintf( outputfile, "%s", output->c_str() ) ;


} // IOUnit::OutData()

DataManager::DataManager( std::string * inputFilePath ) {

  mIO_Unit = new IOUnit( inputFilePath ) ;

  mColorBalance = this->GetIO_Unit()->InputData( mALPHA, mBETA, mOMEGA ) ;

}  // DataManager::DataManager()

IOUnit * DataManager::GetIO_Unit() {

  return this->mIO_Unit ;

}  // DataManager::GetIO_Unit()

ColorBalance * DataManager::GetGraphic() {

  return this->mColorBalance ;

}  // DataManaer::GetGraphic()

void DataManager::PrintData() {

  std::string * out = new std::string() ;

  std::vector< int > WindowSort ;
#ifdef DEBUG
  FILE * file = fopen( resultFile->c_str(), "w" ) ;
#endif

  // sort window
  for ( int i = 0; i < mColorBalance->mWindowSize; i++ ) {
    if ( WindowSort.empty() )
      WindowSort.push_back( i ) ;
    else {
      int select = 0 ;
      for ( int j = 0; j < WindowSort.size(); j++ ) {
        if ( mColorBalance->mWindowList[ i ].mColorDiff
            < mColorBalance->mWindowList[ WindowSort[ j ] ].mColorDiff ) {
          break ;
        }
        else
          select = j + 1 ;
      } // for
      if ( select == WindowSort.size() )
        WindowSort.push_back( i ) ;
      else
        WindowSort.insert( WindowSort.begin() + select, i ) ;
    }  // else
  } // for

  for ( int i = 0; i < mColorBalance->mWindowSize; i++ ) {
    char * str = new char[ 50 ] ;
    sprintf( str, "WIN[%d]=%d,%d,%d,%d(%4.2f %4.2f)\n", i + 1,
        mColorBalance->mWindowList[ WindowSort[ i ] ].mPosition[ 0 ]->X,
        mColorBalance->mWindowList[ WindowSort[ i ] ].mPosition[ 0 ]->Y,
        mColorBalance->mWindowList[ WindowSort[ i ] ].mPosition[ 1 ]->X,
        mColorBalance->mWindowList[ WindowSort[ i ] ].mPosition[ 1 ]->Y,
        mColorBalance->mWindowList[ WindowSort[ i ] ].mColorDensityA,
        mColorBalance->mWindowList[ WindowSort[ i ] ].mColorDensityB ) ;
#ifdef DEBUG
    fprintf( file, "%4.3f\t%4.3f\t%4.3f\t%d\n", mColorBalance->mWindowList[ WindowSort[ i ] ].mColorDensityA,
     mColorBalance->mWindowList[ WindowSort[ i ] ].mColorDensityB,mColorBalance->mWindowList[ WindowSort[ i ] ].mColorDiff, WindowSort[ i ] ) ;
#endif
    out->append( str ) ;
  }  // for

  for ( int printNonColor = 0; printNonColor < 2; printNonColor++ ) {
    for ( int i = 0; i < mColorBalance->mGroupList->size(); i++ ) {
      std::string strA ;
      std::string strB ;
      int A = 0 ;
      int B = 0 ;
      std::vector< Vertex * > * temp = ( *( mColorBalance->mGroupList ) )[ i ] ;
      for ( int j = 0; j < temp->size(); j++ ) {
        Component * Ctemp = mColorBalance->mComponentList->at(
            ( *temp )[ j ]->mID ) ;
        char * str = new char[ 50 ] ;

        if ( printNonColor == 0 && ( *temp )[ j ]->mColor == NOCOLOR ) {
          if ( j == 0 )
            out->append( "GROUP\n" ) ;
          sprintf( str, "NO[%d]=%d,%d,%d,%d\n", j + 1,
              Ctemp->mPosition[ LEFT_DOWN ].X, Ctemp->mPosition[ LEFT_DOWN ].Y,
              Ctemp->mPosition[ RIGHT_UP ].X, Ctemp->mPosition[ RIGHT_UP ].Y ) ;
          strA.append( str ) ;
        }  // if

        else if ( printNonColor == 1 ) {
          if ( ( *temp )[ j ]->mColor == RED ) {
            if ( j == 0 )
              out->append( "GROUP\n" ) ;
            A++ ;
            sprintf( str, "CA[%d]=%d,%d,%d,%d\n", A,
                Ctemp->mPosition[ LEFT_DOWN ].X,
                Ctemp->mPosition[ LEFT_DOWN ].Y, Ctemp->mPosition[ RIGHT_UP ].X,
                Ctemp->mPosition[ RIGHT_UP ].Y ) ;
            strA.append( str ) ;
          }  // else if

          else if ( ( *temp )[ j ]->mColor == BLUE ) {
            if ( j == 0 )
              out->append( "GROUP\n" ) ;
            B++ ;
            sprintf( str, "CB[%d]=%d,%d,%d,%d\n", B,
                Ctemp->mPosition[ LEFT_DOWN ].X,
                Ctemp->mPosition[ LEFT_DOWN ].Y, Ctemp->mPosition[ RIGHT_UP ].X,
                Ctemp->mPosition[ RIGHT_UP ].Y ) ;
            strB.append( str ) ;
          }  //else
        } // else if
      }  // for

      out->append( strA ) ;
      out->append( strB ) ;

    }  // for
  } // for

  mIO_Unit->OutputData( out ) ;
  //printf( "%s", out->c_str() ) ;

} // DataManager::PrintData()

