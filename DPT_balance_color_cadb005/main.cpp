/*
 * main.cpp
 *
 *  Created on: 2015年4月22日
 *      Author: LuoJing-Yuan
 */

#include "DataManager.h"
#include <time.h>



#define TIMELIMIT 700
#define REPEATTIME 250

int main( int argc, char*argv[] ) {
  srand(time(NULL));

  if ( argc != 3 ) {
    printf( "Incorrect number of argument\n" ) ;
    return -1 ;
  }  // if

  else {

    std::string * str = new std::string ;
    str->append( argv[ 1 ] ) ;

    DataManager * DM = new DataManager( str ) ; // input file path, if path is null, scan from console.
    str = new std::string ;

    str->append( argv[ 2 ] ) ;

    DM->mIO_Unit->SetOutputFilePath( str ) ;

    DM->GetGraphic()->StartVisitVertex( true, -1 ) ;

    DM->mColorBalance->mWindowBound = DM->GetGraphic()->GetWindowBound() ;

    clock_t start, end;
    int exctime = 0 ;
    start = clock();
    double nowDiff = DBL_MAX ;

    for ( int t = 0 ; exctime < TIMELIMIT  && t <  REPEATTIME ; ) {
      DM->GetGraphic()->DoOptimizeColorBalance( 0 ) ;
      int time = rand()%7+3 ;
      for ( int i = 0; i < time ; i++ )
        DM->GetGraphic()->DoOptimizeColorBalance( 1 ) ;
      DM->mColorBalance->CopyColorList() ;

      //printf( "Diff: %.3f\n", DM->mColorBalance->mBestList->mDiff ) ;
      end = clock();
      if ( ( nowDiff - DM->mColorBalance->mBestList->mDiff ) < 0.001  )
        t++ ;
      else
        t = 0 ;
      nowDiff = DM->mColorBalance->mBestList->mDiff ;
      exctime =  (end - start) / CLOCKS_PER_SEC ;
      //printf("Sec: %d, repeat: %d\n", exctime, t );

    } // for

    DM->GetGraphic()->DoOptimizeColorBalance( 2 ) ;

    DM->mColorBalance->CalculateWindowDensity() ;

    DM->PrintData() ;

    return 0 ;

  } // else

}  // main()

